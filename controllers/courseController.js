const User = require("../models/user")
const Course = require("../models/course")
const auth = require("../auth")
const bcrypt = require("bcrypt") //used to encrypt user passwords

module.exports.registerCourse = (reqBody) =>{
    let newCourse = new Course({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
    })
    return newCourse.save().then((saved, error) => {
        if(error){
            console.log(error)
            return false
        }else{
            return true
        }
    })
}
