const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const courseController = require("../controllers/courseController")
const course = require("../models/course.js")

router.post("/register", (req,res) => {
    courseController.registerCourse(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/enroll", (req,res) => {
    courseController.enrollCourse(req.body).then(resultFromController => res.send(resultFromController))
})

module.exports = router